package clases;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.lang.Integer;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
        
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author joaqu
 * 
 * 
 * C = {10, 7, 1, 5, 10, 6, 7, 8, 4, 6, 3, 9, 5, 12, 2, 3, 11, 10, 9, 4}   [n = 20]

Mediana parcial de los primeros   5 elementos:  7

Mediana parcial de los primeros 10 elementos:  6.5

Mediana parcial de los primeros 15 elementos:  6

Mediana parcial de los primeros 20 elementos:  6.5 (que en este caso, es también la mediana total)
 */

// [1, 2, 3, 4, 5, 5, 6, 6, 7, 7, 8, 9, 10, 10, 12]


public class Test {
    
    // returns an Integer equal to 0 if ascHead = dscHead
    // returns an Integer greater than 0 if ascHead > dscHead
    // returns an Integer lesser than 0 if ascHead < dscHead
    public int compareHeads(int ascHead, int dscHead){
        return (Integer.compare(ascHead, dscHead));
    }
    
    // Returns the average of both heads.
    public static float headsAverage(TSBHeap h0, TSBHeap h1){
        Integer h0HeadValue = (Integer)h0.get();
        Integer h1HeadValue = (Integer)h1.get();
        
        float h0Head = (float)h0HeadValue;
        float h1Head = (float)h1HeadValue;
        
        return ((h0Head + h1Head) / 2);
    }
    
    // Compares the size between both heaps h0 and h1.
    // Returns 0 if both have the same ammount of elements.
    // Returns 1 if h0 have more elements.
    // Returns -1 if h1 have more elements.
    public static int compareSize(TSBHeap h0, TSBHeap h1){
        int difference = Integer.compare(h0.size(), h1.size());
        
        switch (difference){
            case 0: return 0;
            case 1: return 1;
            case -1: return -1;
        }
        
        return 0;
    }
    
    // Returns true if size of Heap h is even, false otherwise.
    public boolean evenHeapSize(TSBHeap h){
        int remainder = h.size() % 2;
        
        return remainder == 0;
        
    }
    
    // Method that loads both heaps and return them in a Map.
    public static Map loadHeaps(Integer[] ints, TSBHeap h0, TSBHeap h1){
        boolean firstTime = true;
        Map<String, TSBHeap> heaps = new HashMap<>();
        
        for(Integer elem: ints){

            if (firstTime) {
                h0.add(elem);
                firstTime = false;
            }else{
            switch (compareSize(h0,h1)){
                case 0:
                    if(elem < (Integer)h1.get()){
                    h1.add(elem);
                }else{
                    h0.add(elem);
                }
                    continue;
                
                case -1:
                    if (elem < (Integer)h1.get()){
                        h0.add(h1.remove());
                        h1.add(elem);
                    }else{
                        h0.add(elem);
                    }
                    continue;

                case 1:
                    if (elem > (Integer)h0.get()){
                        //Comparable h0HeadValue = h0.remove();
                        h1.add(h0.remove());
                        h0.add(elem);
                    }else{
                        h1.add(elem);
                    }
                    continue;
                    }
                }
            
            
        }
        
        heaps.put("asc", h0);
        heaps.put("dsc", h1);
        
        return heaps;
   }
    
    
    public static void main(String args[])throws Exception
    {
        String st;
        int cant = 0;
        Scanner myInput = new Scanner(System.in);
        List<Integer> valuesList = new ArrayList<Integer>();
        Map<String, TSBHeap> heaps = new HashMap<>();           // HashMap que almacena los heaps <"(asc,dsc)",(h0,h1)>
        

        File file = new File("list[100000].txt");

        BufferedReader br = new BufferedReader(new FileReader(file));

        System.out.println("Ingresa cantidad de elementos para calcular la mediana parcial: \n");
        int medianaParcial = myInput.nextInt();
        Integer[] valuesArray = new Integer[medianaParcial];
        TSBHeap<Integer> h0 = new TSBHeap((medianaParcial/2)+1, true);        //heap ascendente (menor en la cima)
        TSBHeap<Integer> h1 = new TSBHeap((medianaParcial/2)+1, false);       //heap descendente (mayor en la cima)
        
        // Parseamos y cargamos nuestra lista de integers.
        while ((st = br.readLine()) != null && cant < medianaParcial){
            valuesList.add(Integer.parseInt(st));
            ++cant;
        }
        valuesArray = valuesList.toArray(valuesArray);

        System.out.println("*******************************************");
        heaps = loadHeaps(valuesArray, h0, h1);
        System.out.println("Heaps cargados correctamente");
        
        System.out.println("*************");
        if(compareSize(h0,h1) == 0){
            System.out.println("La mediana parcial es: " + headsAverage(h0,h1));
        }else{
            if(h0.size() > h1.size()){
                System.out.println("La mediana parcial es: " + (Integer)h0.get());
            }else{
                System.out.println("La mediana parcial es: " + (Integer)h1.get());
            }
        }
           
            
            
    }
    
    
    
}
